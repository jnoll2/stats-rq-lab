<!--
If you see this comment, you are reading the wrong file!
View #filename(instructions.pdf) or #(instructions.html)
per instructions in #filename(README.txt).
-->

---
author: #module_leader
date: #date
documentclass: hitec
fontfamily: opensans
fontsize: 12
...
# Overview

This deliverable is the Research Question component of the
coursework.  You will submit your research question using Git, so it
is important that you understand how to use Git to commit and upload
files to #githost from the command line, that you invite #module_email
to join your repository, and that you work with the repository whose
URL you submitted via the "Git (Bitbucket) Repository URL" assignment
on Canvas.

Using the #githost web
interface is not acceptable and will result in reduced marks for the
"Process" component  of the coursework.

To submit the Research Question deliverable successfully, you must complete three steps:

1. Identify a dataset, and ensure it is not in use by another group.
2. Formulate a research question, in one of the three forms specified in the Research Questions lecture.
3. Document your research question in a YAML file, and submit your
YAML file and dataset using Git.

Each of these steps is described in detail below.

# Instructions

## Identify a dataset.

This task has two steps:

1. Choose a dataset from the list of allowed Kaggle datasets.
2. Register your choice on the Dataset Sharepoint list.

## Choose a dataset

*Note: you will not be able to do this after the dataset choice
  deadline. After the deadline, a dataset will be assigned to you.*

1. Visit the Dataset Sharepoint list:

    \url{#dataset_site_url}


2. Browse the available topics.  Only the topics shown on the
Sharepoint list are allowed.
3. Check the description of interesting datasets on Kaggle, by
following the link in the Sharepoint list.
4, Agree on a dataset.  Discuss your preferences, and their
preferences, with your teammates, and decide on _one_ dataset.
5. *One* member of the group should put your group name ("a_groupNNN" or "b_groupNNN") on the
Sharepoint list, under the "Student Group" column.

    Your choice *must* be on the Sharepoint list.
    
    a) Find the row associated with your dataset.

        ![Sharepoint list](dataset_list_screenshots/sharepoint-list.png){width=100%}

    a) Click on the row associated with your dataset.

        ![Sharepoint list edit](dataset_list_screenshots/edit-row.png){width=100%}

    a) Double click "Select an option."

        ![Sharepoint list edit](dataset_list_screenshots/add-group.png){width=100%}

    a) Find your group in the dropdown.

        ![Sharepoint list edit](dataset_list_screenshots/group-dropdown.png){width=100%}

    a) Make your reservation.

        ![Sharepoint list edit](dataset_list_screenshots/dataset-taken.png){width=100%}

Once you have placed your group number in the Student Group cell, the
module team will be alerted.

Periodically we will circulate a list of dataset allocations on Slack;
this is your confirmation of your reservation.

We will handle your requests in batches, so please be patient.

If your group does not appear on the list posted to Slack, and you
have entered your group on the Sharepoint list, please wait at least
36 hours before posting a query.

## Download your dataset

1. Download the CSV file allocated to your group from Kaggle.

    Note: some datasets on Kaggle have several associated CSV files;
    be sure you download the one you have been allocated.
    
2. If you have not done so already, clone your group repository.

3. Copy the CSV file to your group's Git workspace.  Do **NOT** change
the name!

    **Is your dataset larger than 5mb?**
    
    Run:
    
    ~~~~~ {.sh}
    Rscript path/to/stats-rq-lab/reduce_size.R your-datafile.csv
    ~~~~~    

    Note: if you are using Windows, #command(Rscript) may not be in
    your #code(PATH) environment variable; if, so the above invocation
    needs to be modified to use the _absolute path_ to
    #command(Rscript).
    
    If you have installed _R_ version 4.3.2, this will look something like:

    \footnotesize

    ~~~~~ {.sh}
    C:"\Program Files\R\R-4.3.2\bin\Rscript.exe" path/to/stats-rq-lab/reduce_size.R  your-datafile.csv
    ~~~~~    

    \normalsize
    
    You will need to change '4.3.2' to the actual version of R you
    have installed.  To do this, open a #command(CMD) window, then
    navigate to your _R_ installation:
    
    ~~~~~ {.sh}
    cd C:"\Program Files\R\
    dir
    ~~~~~
    
    You will see a directory named after the specific version of _R_
    you have installed.
    

4. Add the CSV file to the next commit (use #command(git add) followed by the
(reduced) filename).

    Note: if your CSV file is over 5 MB, use the
    #filename(reduce_size.R) script (see above) to reduce the size,
    then add the _reduced_ CSV file (not the original large CSV file)
    *before* you commit it.

5. Commit  (use #command(git commit) and include a meaningful commit
log message).





## Formulate a Research Question

#. Look at the columns in your dataset.  These are the variables you
 will use when formulating your research question.

#. Identify the *meaning* of the variable names.  This will help you
 formulate a research question.

#. Determine which variable will be your _independent_ variable (the
 one that determines the outcome), and which variable will be your
 _dependent_ variable (the one that represents the _value_ of the outcome).

#. Identify the *kind* of data represented by the independent and dependent variables:
 *interval*, *ordinal*, or *nominal*.  This will determine what kind
 of analysis you can do:

    * If both are interval or ordinal, you may use correlation (but
      only if the independent variable has at least eight distinct
      values).

    * If the _independent_ variable is nominal, or is ordinal with less
      than eight (8) values, but the _dependent_ variable is interval, you
      may use comparison of _means_ (or medians).

    * If the _independent_ variable is nominal, or is ordinal with less
      than eight (8) values, and the _dependent_ variable is an interval
      value representing a proportion (fraction or percentage), you
      should use comparison of _proportions_.

    The Research Questions lecture recordings and notes discuss these
    differences in some detail.  Also, you might want to read the
    marking rubric (#filename(rubric.xlsx)) to see how we will mark
    your research question deliverable.





## Document and Submit your Research Question


#. If you haven't already done so, clone your group's #githost repository into a local workspace.

#. Change directories to the directory of your local workspace.

#. Configure your email address.  From the command line, type

        git config user.email st22abc@herts.ac.uk

    Be sure to user _your_ Herts email address, so your contributions
    to your project are recorded.

#. Configure your user name.  From the command line, type

        git config user.name "Stig Tracy"
	
    Use your name as it appears on Canvas.

### Edit #filename(research_question.yml)

**CAUTION** #filename(research_question.yml) is a "semi-structured"
  file in YAML format.  YAML is a data serialization language that
  uses key-value pairs, similar to those in Windows "INI" files.

Pay careful attention to whitespace, spelling, capitalization, and structure in the instructions below.

#. Copy #filename(research_question.yml) from this instructions workspace to your group workspace.

#. Change to your group workspace, and open #filename(research_question.yml)
 (using Notepad++ or your favorite text editor.  Do  **NOT** use  Windows Notepad!).

#. Write your group name after the #code(group: ) key.

#. Write the names (but **NOT** student IDs) of each group member,
separated by a comma (','), 
within the square brackets after the #code(members: ) key.

#. Write the general topic area from which you derived your research
 question after the #code(topic: ) key.

#. Write your research question after the #code(RQ: ) key.  Be sure it
 is an actual _question_, written following one of the three question
 templates specified in the Research Question lecture recording and
 notes.

    Note: groups who do not express their research question using one of the three question templates are unlikely to pass this assignment.

#. Write the null hypothesis for your research question after the
#code(null-hypothesis: ) key.

#. Write the alternative hypothesis for your research question after the
#code(alt-hypothesis: ) key.

#. Write the URL of your dataset after the #code(dataset-url: ) key.
This *must* be the URL of your allocated dataset (from #dataset_site_name)

#. Write the name of your dataset _file_ after the #code(dataset-file: )
 key.  Be sure it is spelled exactly as it appears on #dataset_site_name
 *AND* in your repository.

#. Load your dataset into R using the #command(read.csv()) function:

        > d <- read.csv("our_dataset_)

    (Replace "our_dataset" with the _actual name of your dataset file_).

#. Print the column names of your dataset using the #command(colnames)
 function:

        > colnames(d)

#. Copy the command invocation and its output, and paste it into the space under the
 #code(columns: |) key.

#. Insert *two* spaces before every line in the output you pasted into the space under the
  #code(columns: |) key.
  
#. Add a newline (carriage return) character at the end of your file.

#. Proofread your file to be sure you have spelled everything
correctly, that you have expressed your research question as a
question, and that you have followed YAML syntax (Wikipedia has a nice
summary of YAML syntax (https://en.wikipedia.org/wiki/YAML) if you
are curious).  Also, the #filename(validate_yaml.R) script
can detect syntax errors in YAML files: run

    Rscript validate_yaml.R research_question.yml
    
    and _pay attention to the output_.

#. Commit your changes using #command(git commit -m "COMMIT LOG
 MESSAGE")  (replace "COMMIT LOG MESSAGE" with an actual, _meaningful_ commit log message).

#. _Validate_ the syntax of your #filename(research_question.yml) file
 using the #filename(validate_yaml.R) script in this respository:
 
     Rscript validate_yaml.R research_question.yml

#. Push your workspace to #githost by the deadline (23:59 #cw_rq_due).

# Notes


#. YAML is a _data serialization_ language.  As such, the syntax of
   the #filename(research_question.yml) file is important:

    * Keys must be spelled *exactly* as shown, with *exactly* the same
      capitalization and spelling.
    * All keys *must* be followed by a colon (':') then a space.
    * The vertical bar after the #code(columns: ) key is meaningful: it means
      the indented text that follows should be formatted exactly as
      written.
    * You *must* indent the text after the #code(columns: ) key by two spaces.

#. Groups who submit YAML files with syntax errors will receive 0  marks on this deliverable.


#. Use the #filename(validate_yaml.R) script to validate the syntax of
your YAML file before submitting:

    Rscript validate_yaml.R research_question.yml
    
    Pay attention to the output, to be sure there are no syntax
    errors, and *all* fields are recognized and have the expected
    values.

    Note: #command(Rscript) might not be in your path if you are using
    Windows.  If so, follow the instructions for running
    #filename(reduce_size.R). 


#. There are only _three_ acceptable formats for research questions in
 *this module* (7COM1085--Team Research and Development Project).
 These are described in detail in the Research Questions lecture
 recording and accompanying notes.

    Be sure you  understand the three formats *before* you write your
research question.

#. The marking rubric (#filename(rubric.xlsx)) shows how we will mark
 your #filename(research_question.yml); test yourself before submitting.


# Example

The following example asks about the relationship between happiness
and generosity (do NOT use the same question for your deliverable!).

~~~~~
#include(example.yml)
~~~~~

    

# Want feedback in advance of the deadline?

Do you want us to comment on your RQ before you submit?

1. Prepare exactly _three_ slides using the
#filename(Research_Question_Slide_Template.pptx) in this repository.
    
2. Sign up on Canvas for a presentation slot (follow the _Calendar_
link in
the far left column).  Be sure you have ticked "7COM1079-0901-2023" under
the "CALENDARS" list on the right side.

4. Present your slides during your presentation slot.

**CAUTION**: groups who miss their presentation slot will be assessed
  a five (5) point penalty on their RQ deliverable.  
