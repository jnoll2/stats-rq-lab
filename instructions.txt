Research Question Assignment
Dr. John Noll
Friday, 10 Nov, 2023

Overview

This deliverable is the Research Question component of the coursework.
You will submit your research question using Git, so it is important
that you understand how to use Git to commit and upload files to
Bitbucket from the command line, that you invite 7com1079@herts.ac.uk to
join your repository, and that you work with the repository whose URL
you submitted via the “Git (Bitbucket) Repository URL” assignment on
Canvas.

Using the Bitbucket web interface is not acceptable and will result in
reduced marks for the “Process” component of the coursework.

To submit the Research Question deliverable successfully, you must
complete three steps:

1.  Identify a dataset, and ensure it is not in use by another group.
2.  Formulate a research question, in one of the three forms specified
    in the Research Questions lecture.
3.  Document your research question in a YAML file, and submit your YAML
    file and dataset using Git.

Each of these steps is described in detail below.

Instructions

Identify a dataset.

This task has two steps:

1.  Choose a dataset from the list of allowed Kaggle datasets.
2.  Register your choice on the Dataset Sharepoint list.

Choose a dataset

Note: you will not be able to do this after the dataset choice deadline.
After the deadline, a dataset will be assigned to you.

1.  Visit the Dataset Sharepoint list:

2.  Browse the available topics. Only the topics shown on the Sharepoint
    list are allowed.

3.  Check the description of interesting datasets on Kaggle, by
    following the link in the Sharepoint list. 4, Agree on a dataset.
    Discuss your preferences, and their preferences, with your
    teammates, and decide on one dataset.

4.  One member of the group should put your group name (“a_groupNNN” or
    “b_groupNNN”) on the Sharepoint list, under the “Student Group”
    column.

    Your choice must be on the Sharepoint list.

    a)  Find the row associated with your dataset.

        [Sharepoint list]

    b)  Click on the row associated with your dataset.

        [Sharepoint list edit]

    c)  Double click “Select an option.”

        [Sharepoint list edit]

    d)  Find your group in the dropdown.

        [Sharepoint list edit]

    e)  Make your reservation.

        [Sharepoint list edit]

Once you have placed your group number in the Student Group cell, the
module team will be alerted.

Periodically we will circulate a list of dataset allocations on Slack;
this is your confirmation of your reservation.

We will handle your requests in batches, so please be patient.

If your group does not appear on the list posted to Slack, and you have
entered your group on the Sharepoint list, please wait at least 36 hours
before posting a query.

Download your dataset

1.  Download the CSV file allocated to your group from Kaggle.

    Note: some datasets on Kaggle have several associated CSV files; be
    sure you download the one you have been allocated.

2.  If you have not done so already, clone your group repository.

3.  Copy the CSV file to your group’s Git workspace. Do NOT change the
    name!

    Is your dataset larger than 5mb?

    Run:

        Rscript path/to/stats-rq-lab/reduce_size.R your-datafile.csv

    Note: if you are using Windows, Rscript may not be in your PATH
    environment variable; if, so the above invocation needs to be
    modified to use the absolute path to Rscript.

    If you have installed R version 4.3.2, this will look something
    like:

        C:"\Program Files\R\R-4.3.2\bin\Rscript.exe" path/to/stats-rq-lab/reduce_size.R  your-datafile.csv

    You will need to change ‘4.3.2’ to the actual version of R you have
    installed. To do this, open a CMD window, then navigate to your R
    installation:

        cd C:"\Program Files\R\
        dir

    You will see a directory named after the specific version of R you
    have installed.

4.  Add the CSV file to the next commit (use git add followed by the
    (reduced) filename).

    Note: if your CSV file is over 5 MB, use the reduce_size.R script
    (see above) to reduce the size, then add the reduced CSV file (not
    the original large CSV file) before you commit it.

5.  Commit (use git commit and include a meaningful commit log message).

Formulate a Research Question

1.  Look at the columns in your dataset. These are the variables you
    will use when formulating your research question.

2.  Identify the meaning of the variable names. This will help you
    formulate a research question.

3.  Determine which variable will be your independent variable (the one
    that determines the outcome), and which variable will be your
    dependent variable (the one that represents the value of the
    outcome).

4.  Identify the kind of data represented by the independent and
    dependent variables: interval, ordinal, or nominal. This will
    determine what kind of analysis you can do:

    -   If both are interval or ordinal, you may use correlation (but
        only if the independent variable has at least eight distinct
        values).

    -   If the independent variable is nominal, or is ordinal with less
        than eight (8) values, but the dependent variable is interval,
        you may use comparison of means (or medians).

    -   If the independent variable is nominal, or is ordinal with less
        than eight (8) values, and the dependent variable is an interval
        value representing a proportion (fraction or percentage), you
        should use comparison of proportions.

    The Research Questions lecture recordings and notes discuss these
    differences in some detail. Also, you might want to read the marking
    rubric (rubric.xlsx) to see how we will mark your research question
    deliverable.

Document and Submit your Research Question

1.  If you haven’t already done so, clone your group’s Bitbucket
    repository into a local workspace.

2.  Change directories to the directory of your local workspace.

3.  Configure your email address. From the command line, type

         git config user.email st22abc@herts.ac.uk

    Be sure to user your Herts email address, so your contributions to
    your project are recorded.

4.  Configure your user name. From the command line, type

         git config user.name "Stig Tracy"

    Use your name as it appears on Canvas.

Edit research_question.yml

CAUTION research_question.yml is a “semi-structured” file in YAML
format. YAML is a data serialization language that uses key-value pairs,
similar to those in Windows “INI” files.

Pay careful attention to whitespace, spelling, capitalization, and
structure in the instructions below.

1.  Copy research_question.yml from this instructions workspace to your
    group workspace.

2.  Change to your group workspace, and open research_question.yml.

3.  Write your group name after the group: key.

4.  Write the names (but NOT student IDs) of each group member,
    separated by a comma (‘,’), within the square brackets after the
    members: key.

5.  Write the general topic area from which you derived your research
    question after the topic: key.

6.  Write your research question after the RQ: key. Be sure it is an
    actual question, written following one of the three question
    templates specified in the Research Question lecture recording and
    notes.

    Note: groups who do not express their research question using one of
    the three question templates are unlikely to pass this assignment.

7.  Write the null hypothesis for your research question after the
    null-hypothesis: key.

8.  Write the alternative hypothesis for your research question after
    the alt-hypothesis: key.

9.  Write the URL of your dataset after the dataset-url: key. This must
    be the URL of your allocated dataset (from Sharepoint)

10. Write the name of your dataset file after the dataset-file: key. Be
    sure it is spelled exactly as it appears on Sharepoint AND in your
    repository.

11. Load your dataset into R using the read.csv() function:

         > d <- read.csv("our_dataset_)

    (Replace “our_dataset” with the actual name of your dataset file).

12. Print the column names of your dataset using the colnames function:

         > colnames(d)

13. Copy the command invocation and its output, and paste it into the
    space under the columns: | key.

14. Insert two spaces before every line in the output you pasted into
    the space under the columns: | key.

15. Add a newline (carriage return) character at the end of your file.

16. Proofread your file to be sure you have spelled everything
    correctly, that you have expressed your research question as a
    question, and that you have followed YAML syntax (Wikipedia has a
    nice summary of YAML syntax (https://en.wikipedia.org/wiki/YAML) if
    you are curious). Also, the validate_yaml.R script can detect syntax
    errors in YAML files: run

    Rscript validate_yaml.R research_question.yml

    and pay attention to the output.

17. Commit your changes using git commit -m "COMMIT LOG  MESSAGE".

18. Validate the syntax of your research_question.yml file using the
    validate_yaml.R script in this respository:

    Rscript validate_yaml.R research_question.yml

19. Push your workspace to Bitbucket by the deadline (23:59 2023-11-10).

Notes

1.  YAML is a data serialization language. As such, the syntax of the
    research_question.yml file is important:

    -   Keys must be spelled exactly as shown, with exactly the same
        capitalization and spelling.
    -   All keys must be followed by a colon (‘:’) then a space.
    -   The vertical bar after the columns: key is meaningful: it means
        the indented text that follows should be formatted exactly as
        written.
    -   You must indent the text after the columns: key by two spaces.

2.  Groups who submit YAML files with syntax errors will receive 0 marks
    on this deliverable.

3.  Use the validate_yaml.R script to validate the syntax of your YAML
    file before submitting:

    Rscript validate_yaml.R research_question.yml

    Pay attention to the output, to be sure there are no syntax errors,
    and all fields are recognized and have the expected values.

    Note: Rscript might not be in your path if you are using Windows. If
    so, follow the instructions for running reduce_size.R.

4.  There are only three acceptable formats for research questions in
    this module (7COM1085–Team Research and Development Project). These
    are described in detail in the Research Questions lecture recording
    and accompanying notes.

    Be sure you understand the three formats before you write your
    research question.

5.  The marking rubric (rubric.xlsx) shows how we will mark your
    research_question.yml; test yourself before submitting.

Example

The following example asks about the relationship between happiness and
generosity (do NOT use the same question for your deliverable!).

    # This file is in YAML format.  You must ensure that the punctuation
    # and whitespace is preserved.  Do NOT use Windows Notepad to edit this
    # file; use a real programming editor like Notepad++ instead.

    # Write your group name exactly as it appears on Canvas.
    group: A_group 999

    # Write the names of your group members as a comma-separated list
    # within the square brackets.
    members: [Luke Bisee, Harly Werkin, Notta Klu, Ben Gonna, Abbey Sent]

    # Write your general topic.  
    topic: Happiness 

    # Write your research question, following one of the three allowed templates.
    # Be sure your question is all on one line.
    RQ: Is there a correlation between happiness and generosity?

    # Write your independent and dependent variables.  These should be the
    # column names (reported by R colnames() where these variables are
    # found in your dataset's CSV file.
    # Note: if your question is about comparison of means, medians, or
    # proportions, the independent variable column is the one that
    # contains the values (at least two) that divide the population into
    # subsets.
    independent-var: Generosity
    dependent-var: Happiness.Score

    # Write the null hypothesis for your research question, again ensuring it is all on one line.
    null-hypothesis: There is no correlation between happiness and generosity.

    # Write the alternative hypothesis for your research question, again ensuring it is all on one line.
    alt-hypothesis: There is a correlation between happiness and generosity.

    # Write the dataset ID (KNNN):
    dataset-id: K999

    # Write the dataset URL.  Be sure it has the allowed hostname.
    dataset-url: "https://Sharepoint/juiche/happiness"

    # Write the dataset CSV filename, exactly as it appears on the Sharepoint list
    # (which should be the same as the one in your repository!)
    dataset-file: 2015.csv



    # Paste the output of loading your dataset into R, and executing the
    # colnames() function, into the space below the `columns: |` line.
    # Put two spaces *before* every line you pasted.
    columns: |
      > d<-read.csv("2015.csv")
      > colnames(d)
       [1] "Country"                       "Region"                       
       [3] "Happiness.Rank"                "Happiness.Score"              
       [5] "Standard.Error"                "Economy..GDP.per.Capita."     
       [7] "Family"                        "Health..Life.Expectancy."     
       [9] "Freedom"                       "Trust..Government.Corruption."
      [11] "Generosity"                    "Dystopia.Residual"            

Want feedback in advance of the deadline?

Do you want us to comment on your RQ before you submit?

1.  Prepare exactly three slides using the
    Research_Question_Slide_Template.pptx in this repository.

2.  Sign up on Canvas for a presentation slot (follow the Calendar link
    in the far left column). Be sure you have ticked
    “7COM1079-0901-2023” under the “CALENDARS” list on the right side.

3.  Present your slides during your presentation slot.

CAUTION: groups who miss their presentation slot will be assessed a five
(5) point penalty on their RQ deliverable.
